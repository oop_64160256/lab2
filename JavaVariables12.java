public class JavaVariables12{
    public static void main(String[]args){
        /* Exercise:
            Create a variable named "carName" and assign the 
            value "Volvo" to it. */
        String carName = "Volvo";
    }
}